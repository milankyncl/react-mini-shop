import { createStore } from 'redux'

const initialState = {
		loading: true,
		complexity: 99,
		cartItems: [],
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
			case 'cart/add-item-to-cart':
					return {
							...state,
							cartItems: [
									...state.cartItems,
									action.payload.product,
							],
					};

			default:
					return state;
	}
};

export const store = createStore(reducer);
