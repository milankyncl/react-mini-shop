import {useSelector} from "react-redux";
import {useEffect, useState} from "react";

export function useCart() {
		const [loading, setLoading] = useState(true);
		const cartItems = useSelector(state => state.cartItems);

		useEffect(() => {
				setTimeout(() => {
						setLoading(false);
				}, 2000)
		}, []);

		return {
				cartIsLoading: loading,
				cartItems,
		}
}
