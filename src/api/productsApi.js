
export async function fetchProducts() {
		try {
				const response = await fetch(`https://fakestoreapi.com/products`, {
						method: 'GET',
				});
				if (response.status !== 200) {
						throw new Error('xxx');
				}
				return await response.json();
		} catch (e) {
				console.error('error fetching products', e);
		}
}


export async function fetchProductById(id) {
		try {
				const response = await fetch(`https://fakestoreapi.com/products/${id}`, {
						method: 'GET',
				});
				if (response.status !== 200) {
						throw new Error('product not found');
				}
				return await response.json();
		} catch (e) {
				console.error('error fetching products', e);
		}
}
