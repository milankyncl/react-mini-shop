import React from 'react';
import ProductListItem from "./ProductListItem";

export default function ProductsList(props) {
		const { products, handleProductClick } = props;

		return products.map((product, i) => (
				<ProductListItem
						key={i}
						product={product}
						handleClick={handleProductClick}
				/>
		))
}
