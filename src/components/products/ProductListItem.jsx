import React from 'react';

import './ProductListItem.css';

export default function ProductListItem(props) {
		const { product, handleClick } = props;
		return (
				<div className="product-item" onClick={(e) => handleClick(product)}>
						<img alt={product.title} src={product.image} className="product-image" />
						<span className="product-name">{product.title}</span>
						<span className="product-price">{product.price}$</span>
				</div>
		)
}
