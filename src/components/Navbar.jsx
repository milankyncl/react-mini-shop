import React from 'react';
import { Link } from "react-router-dom";

import './Navbar.css';
import {useCart} from "../hooks/useCart";


export default function Navbar() {
		const { cartIsLoading, cartItems } = useCart();

		return (
				<div className="navbar">
						<ul>
								<li>
										<Link to="/">Domů</Link>
								</li>
								<li>
										<Link to="/produkty">Produkty</Link>
								</li>
								{!cartIsLoading && (
										<li>
												Můj košík ({cartItems.length})
										</li>
								)}
						</ul>
				</div>
		)
}
