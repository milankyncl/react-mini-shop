import React, { useState, useEffect } from 'react';

import './ProductsPage.css';
import ProductsList from "../components/products/ProductsList";
import {fetchProducts} from "../api/productsApi";
import {useHistory} from "react-router";

export default function ProductsPage() {
		const history = useHistory();

		const [loading, setLoading] = useState(true);
		const [products, setProducts] = useState([]);

		useEffect(() => {
				(async () => {
						const fetchedProducts = await fetchProducts();
						setProducts(fetchedProducts);
						setLoading(false);
				})();
		}, []);

		const handleProductClick = (product) => history.push(`/produkty/${product.id}`);

		return (
				<>
						{loading ? (
								<span className="loading">
										Loaduju produkty...
								</span>
						) : (
								<ProductsList
										products={products}
										handleProductClick={handleProductClick}
								/>
						)}
			  </>
		);
}
