import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from "react-router";
import { fetchProductById } from "../api/productsApi";
import { useDispatch } from "react-redux";

const addToCartActionCreator = (product) => ({
		type: 'cart/add-item-to-cart',
		payload: {
				product,
		},
});

export default function ProductDetailPage() {
		const params = useParams();
		const history = useHistory();
		const dispatch = useDispatch();

		const [loading, setLoading] = useState(true);
		const [product, setProduct] = useState([]);

		const productId = params.id ? parseInt(params.id) : null;

		useEffect(() => {
				if (!productId) {
						history.push('/');
						return;
				}
				(async () => {
						const fetchedProduct = await fetchProductById(productId);
						setProduct(fetchedProduct);
						setLoading(false);
				})();
		}, [productId, history]);

		if (loading) {
				return <>Loading...</>;
		}

		return (
				<div className="product-detail">
						<h1>{product.title}</h1>

						<img className="product-image" src={product.image} alt={product.title} />

						<button type="button" onClick={() => dispatch(addToCartActionCreator(product))}>Přidat do košíku</button>
			  </div>
		);
}
