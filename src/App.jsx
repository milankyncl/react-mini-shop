import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import { Provider } from 'react-redux';

import Navbar from './components/Navbar';

import './App.css';
import Home from "./pages/Home";
import ProductsPage from "./pages/ProductsPage";
import ProductDetailPage from "./pages/ProductDetailPage";

import { store } from './redux/store';

function App() {
  return (
      <Provider store={store}>
          <Router>
              <Navbar />
              <Switch>
                  <Route exact path="/produkty/:id">
                      <ProductDetailPage />
                  </Route>
                  <Route exact path="/produkty">
                      <ProductsPage />
                  </Route>
                  <Route path="/">
                      <Home />
                  </Route>
              </Switch>
          </Router>
      </Provider>
  );
}

export default App;
